import React, { useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
// Redux
import { useDispatch, useSelector } from "react-redux";
import { fetchGetExercises } from "./actions/exercise";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchGetExercises());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const { exercises } = useSelector(state => state.exerciseReducer);

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
