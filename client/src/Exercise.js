import React, { useEffect, useState, memo } from "react";
import "./Exercice.css";
// Redux
import { useDispatch, useSelector } from "react-redux";
import { fetchExec, fetchGetExercises } from "./actions/exercise";
import AceEditor from "react-ace";
import ReactResizeDetector from "react-resize-detector";
import { SET_CODE, NEXT } from "./actions/types";
import "ace-builds/src-noconflict/mode-javascript";
import "ace-builds/src-noconflict/theme-github";
import { useElapsedTime } from "use-elapsed-time";

function Exercise() {
  const dispatch = useDispatch();

  const [editorHeight, setEditorHeight] = useState(400);
  const [editorWidth, setEditorWidth] = useState("100%");
  const [isPlaying, setIsPlaying] = useState(true);

  const { elapsedTime } = useElapsedTime(isPlaying);

  const {
    isFetching,
    index,
    size,
    code,
    name,
    description,
    execResult,
    error,
    isFinished
  } = useSelector(state => state.exerciseReducer);

  useEffect(() => {
    dispatch(fetchGetExercises());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const onResize = (w, h) => {
    setEditorHeight(h);
    setEditorWidth(w);
  };

  useEffect(() => {
    if (isFinished) {
      setIsPlaying(false);
    }
  }, [isFinished]);

  return (
    <div className="App">
      <h1 className="title">{"You Can't JavaScript Under Pressure"}</h1>
      <div className="spaceBetween">
        <h2 className="title">{`${index + 1} / ${size} : ${name}`}</h2>
        <h2 className="time">
          {new Date(elapsedTime * 1000).toISOString().substr(11, 8)}
        </h2>
      </div>
      <div className="resizable">
        <ReactResizeDetector handleWidth handleHeight onResize={onResize} />
        <AceEditor
          className="ace-editor"
          placeholder="Placeholder Text"
          mode="javascript"
          theme="github"
          name="blah2"
          height={editorHeight}
          width={editorWidth}
          // onLoad={this.onLoad}
          onChange={value =>
            dispatch({
              type: SET_CODE,
              value
            })
          }
          fontSize={14}
          showPrintMargin={true}
          showGutter={true}
          highlightActiveLine={true}
          value={code}
          setOptions={{
            enableBasicAutocompletion: false,
            enableLiveAutocompletion: false,
            enableSnippets: false,
            showLineNumbers: true,
            tabSize: 2
          }}
        />
      </div>
      <div className="container">
        {execResult?.success ? (
          <button
            className="subcontainer button"
            onClick={() =>
              dispatch({
                type: NEXT
              })
            }
            disabled={isFetching || isFinished}
          >
            {isFinished ? "End" : "Next"}
          </button>
        ) : (
          <button
            className="subcontainer button"
            onClick={() => {
              dispatch(fetchExec({ index, code }));
            }}
            disabled={isFetching}
          >
            Go
          </button>
        )}
        {isFinished ? (
          <div className="subcontainer">
            <h3>{`Well done, you took ${new Date(elapsedTime * 1000)
              .toISOString()
              .substr(11, 8)} to complete all the ${size} exercices !`}</h3>
          </div>
        ) : (
          <div className="subcontainer">
            {description && <h4>Code as fast as you can! {description}</h4>}
            {error && (
              <>
                <h3 className="red">ERROR</h3>
                <h5 className="red">{error}</h5>
              </>
            )}
            {execResult && (
              <>
                {execResult.results.map(exec => (
                  <h5 className={exec.success ? "green" : "red"}>{`${
                    exec.call
                  } => ${exec.result} ${
                    exec.success
                      ? "SUCCESS"
                      : "FAILED expected " + exec.expected
                  }`}</h5>
                ))}
                {execResult.success ? (
                  <h3 className="green">SUCCESS</h3>
                ) : (
                  <h3 className="red">FAILED</h3>
                )}
              </>
            )}
          </div>
        )}
      </div>
    </div>
  );
}

const MemoizedExercise = memo(Exercise);
export default MemoizedExercise;
