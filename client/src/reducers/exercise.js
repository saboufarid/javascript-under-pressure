import {
  API_START,
  API_END,
  FETCH_GET_EXERCISES,
  SET_EXERCISES_FAILURE,
  SET_EXERCISES_RESULT,
  SET_CODE,
  SET_EXEC_RESULT,
  FETCH_EXEC,
  NEXT,
  SET_EXEC_FAILURE
} from "../actions/types";

const initialState = {
  isFetching: false,
  exercises: null,
  error: null,
  code: null,
  name: null,
  description: null,
  index: 0,
  size: 0,
  execResult: null,
  isClearValidate: false,
  isFinished: false
};

export default function exerciseReducer(state = initialState, action = {}) {
  switch (action.type) {
    case API_START:
      if (
        action.payload === FETCH_GET_EXERCISES ||
        action.payload === FETCH_EXEC
      ) {
        return {
          ...state,
          isFetching: true,
          execResult: null,
          error: null
        };
      }
      break;

    case SET_EXERCISES_RESULT: {
      const exercises = action.payload;
      const { baseCode: code, name, description } = exercises[state.index];
      return {
        ...state,
        exercises,
        code,
        name,
        description,
        size: exercises.length
      };
    }

    case NEXT: {
      const index = state.index + 1;
      if (index === state.size) {
        return {
          ...state,
          isFinished: true
        };
      }
      const { baseCode: code, name, description } = state.exercises[index];
      return {
        ...state,
        index,
        code,
        name,
        description,
        execResult: null
      };
    }

    case SET_EXEC_RESULT:
      return {
        ...state,
        execResult: action.payload
      };

    case SET_CODE:
      return {
        ...state,
        code: action.value
      };

    case SET_EXEC_FAILURE:
    case SET_EXERCISES_FAILURE:
      return {
        ...state,
        error: action.error.stack ? action.error.stack : action.error
      };

    case API_END:
      if (
        action.payload === FETCH_GET_EXERCISES ||
        action.payload === FETCH_EXEC
      ) {
        return {
          ...state,
          isFetching: false
        };
      }
      break;

    default:
      return state;
  }

  return state;
}
