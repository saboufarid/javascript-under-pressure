import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import exerciseReducer from "./exercise";

export default function createReducers(history) {
  const appReducer = combineReducers({
    router: connectRouter(history),
    exerciseReducer
  });
  return appReducer;
}
