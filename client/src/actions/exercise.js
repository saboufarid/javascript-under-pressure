import { apiAction } from "./api";
import {
  FETCH_GET_EXERCISES,
  SET_EXERCISES_RESULT,
  SET_EXERCISES_FAILURE,
  FETCH_EXEC,
  SET_EXEC_RESULT,
  SET_EXEC_FAILURE
} from "./types";

export function fetchGetExercises() {
  return apiAction({
    url: "/exercises",
    method: "GET",
    onSuccess: setExercises,
    onFailure: setExercisesFailure,
    label: FETCH_GET_EXERCISES
  });
}

function setExercises(data) {
  return {
    type: SET_EXERCISES_RESULT,
    payload: data
  };
}

function setExercisesFailure(error) {
  return {
    type: SET_EXERCISES_FAILURE,
    error
  };
}

export function fetchExec(data) {
  return apiAction({
    url: "/exercises",
    method: "POST",
    data,
    onSuccess: setExecResult,
    onFailure: setExecFailure,
    label: FETCH_EXEC
  });
}

function setExecResult(data) {
  return {
    type: SET_EXEC_RESULT,
    payload: data
  };
}

function setExecFailure(error) {
  return {
    type: SET_EXEC_FAILURE,
    error
  };
}
