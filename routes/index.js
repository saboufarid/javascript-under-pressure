import express from "express";
import path from "path";
import exercises from "./exercises";

const mainRouter = express.Router();

mainRouter.use("/exercises", exercises);

mainRouter.use(
  express.static("client/build", {
    index: false
  })
);

mainRouter.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "../client/build/index.html"));
});

export default mainRouter;
