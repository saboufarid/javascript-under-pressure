import express from "express";
const apiRoutes = express.Router();
import exerciseSet from "../exerciseSet";
import httpStatus from "../utils/httpStatus";

let exercises;
(async () => {
  exercises = await exerciseSet();
})();

apiRoutes
  .route("/")
  .get(async (req, res, next) => {
    res.json(exercises);
  })
  .post(async (req, res, next) => {
    // execute an exercice
    const { index = 0, code } = req.body;
    let success = true;
    try {
      const results = [];
      for (const test of exercises[index].tests) {
        const result = eval(code + test.call);

        const ok = result === test.result;
        if (!ok) {
          success = false;
        }

        results.push({
          call: test.call,
          result,
          expected: test.result,
          success: ok
        });
      }
      res.json({
        success,
        results
      });
    } catch (error) {
      console.error(error);
      return res
        .status(httpStatus.NOT_ACCEPTABLE)
        .json({ error: error.message });
    }
  });

export default apiRoutes;
