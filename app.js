import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import mainRouter from "./routes";
import cors from "cors";
// import connectMongo from "./config/mongoconnect";
// const root = require("path").join(__dirname, "client", "build");

const app = express();

// Production enviroment
const isProduction = process.env.NODE_ENV === "production";
app.use(bodyParser.json());

//https debug
app.use(morgan("dev"));

app.use(
  cors({
    origin: function(origin, callback) {
      return callback(null, true);
    },
    credentials: true
  })
);

//Connect Mongo
// connectMongo();

app.use(mainRouter);

// app.use("/*", express.static(root));

const PORT = process.env.PORT || 3001;
app.listen(PORT, () => {
  console.log(`Server is running on PORT ${PORT}`);
});
//
